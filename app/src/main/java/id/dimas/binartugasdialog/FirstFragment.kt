package id.dimas.binartugasdialog

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import id.dimas.binartugasdialog.databinding.FragmentFirstBinding

class FirstFragment : Fragment() {

    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showAlertDialog()
        showAlertDialogWithAction()
        showAlertDialogCustom()
        showAlertDialogFragment()
    }

    private fun showAlertDialog() {
        binding.btnDialog.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Disini Judul")
            dialog.setMessage(
                """
                Boleh panjang-panjang karena memang memberi pesan. 
                Karena kalau judul pendek
            """.trimIndent()
            )
            dialog.show()
        }
    }

    private fun showAlertDialogWithAction() {
        binding.btnDialogWithAction.setOnClickListener {
            val dialog = AlertDialog.Builder(requireContext())
            dialog.setTitle("Disini Judul")
            dialog.setMessage(
                """
                Boleh panjang-panjang karena memang memberi pesan. 
                Karena kalau judul pendek
            """.trimIndent()
            )
            dialog.setPositiveButton("Positif") { dialogInterface, angka ->
                createToast("Ini button positif").show()
            }

            dialog.setNegativeButton("Negatif") { dialogInterface, _ ->

            }

            dialog.setNeutralButton("Netral") { dialogInterface, _ ->

            }
            dialog.setCancelable(false)
            dialog.show()
        }
    }

    private fun createToast(message: String): Toast {
        return Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT)
    }

    private fun showAlertDialogCustom() {
        val customDialog =
            LayoutInflater.from(requireContext()).inflate(R.layout.layout_alert_dialog, null)
        val builder = AlertDialog.Builder(requireContext())

        // membuat textview dan button
        val title = customDialog.findViewById<TextView>(R.id.tv_title)
        val btnOne = customDialog.findViewById<Button>(R.id.btn_one)
        title.text = "Ini Sudah Diubah"
        btnOne.text = "Dismiss"
        builder.setView(customDialog)
        val dialog = builder.create()

        btnOne.setOnClickListener {
            dialog.dismiss()
        }

        binding.btnDialogCustom.setOnClickListener {
            dialog.show()
        }
    }

    private fun showAlertDialogFragment() {
        binding.btnDialogFragment.setOnClickListener {
            val name = binding.edtName.text.toString()
            val dialogFragment = DialogFragment(name)
            dialogFragment.show(requireActivity().supportFragmentManager, null)
        }
    }


}